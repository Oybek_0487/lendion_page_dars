import React, {useState} from 'react';
import icon from "../Image/Icon.png"
import {Link} from "react-scroll";
import strelka from "../Image/strelka.svg"
import gradus from "../Image/gradus.svg"
import watch from "../Image/watch.svg"
function Header() {
  const [isOpen ,setIsOpen] =useState(false)
  return (
    <div>
      <div className={"header"}>
           <div className={"head"}>
            <div className="head-one">
              <img src={icon} alt="#"/>
            </div>
             <div className="head-two">
             <ul className={"two-ul"}>
               <li className={"two-li"}>
                 <Link className="link" to="Our_story" exact="true" duration={10000}>OUR STORY</Link>
               </li>
               <li className={"two-li"}>
                 <Link className="link2" to="Services" exact="true" duration={10000}>SERVICES</Link>
               </li>
               <li className={"two-li"}>
                 <Link className="link2" to="News" exact="true" duration={10000}>NEWS</Link>
               </li>
               <li className={"two-li"}>
                 <Link className="link2" to="Careers" exact="true" duration={10000}>CAREERS</Link>
               </li>
               <li className={"two-li"}>
                 <Link className="link2" to="Footer" exact="true" duration={10000}>CONTACT</Link>
               </li>
             </ul>
               <div className={`nav-toggle ${isOpen && "open"}`} onClick={() => setIsOpen(!isOpen)}>
                 <div className="bar">
                 </div>
               </div>
             </div>
             <div className="head-btn">
               <button className={"my-button"}><Link className="getlink" to="#"  exact="true" duration={10000}>Get in touch</Link>  <img src={strelka} alt="#"/></button>
             </div>
           </div>

        <div className="block">
         <h1>TEAM OF</h1>
          <p>BEST <sup> <img src={gradus} alt="#"/></sup> <br/><span>LOGISTICS</span> <br/>
          SPECIALISTS</p>
          <div className="box">
            <button className={"small"}><Link className="getlink" to="#"  exact="true" duration={10000}>Get in touch</Link>  <img src={strelka} alt="#"/></button>
            <button className={"big"}><img className="image" src={watch} alt="#"/><Link className="getlink" to="#"  exact="true" duration={10000}>Watch the Showreel</Link> </button>
          </div>
        </div>
      </div>


    </div>
  );
}

export default Header;