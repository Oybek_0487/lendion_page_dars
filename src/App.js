import React from "react"
import Header from "./components/Header"
import Footer from "./components/Footer"
import "./main.css"
import Our_story from "./components/Our_story";
import Services from "./components/Services";
import News from "./components/News";
import Careers from "./components/Careers";
function App() {
  return (
  <div>
    <Header />
    <Our_story />
    <Services />
    <News />
    <Careers />
    <Footer />
  </div>

  );
}

export default App;
